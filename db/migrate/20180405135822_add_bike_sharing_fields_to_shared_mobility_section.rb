class AddBikeSharingFieldsToSharedMobilitySection < ActiveRecord::Migration
  def change
    add_column :pre_survey_shared_mobility_characteristics, :aware_of_bike_sharing, :integer
    add_column :pre_survey_shared_mobility_characteristics, :member_of_bike_sharing, :integer
    add_column :pre_survey_shared_mobility_characteristics, :frequency_of_bike_sharing, :integer
  end
end
