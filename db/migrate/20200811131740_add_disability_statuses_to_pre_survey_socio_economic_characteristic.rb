class AddDisabilityStatusesToPreSurveySocioEconomicCharacteristic < ActiveRecord::Migration
  def change
    add_column :pre_survey_socio_economic_characteristics, :disability_status_metro, :integer, limit: 4
    add_column :pre_survey_socio_economic_characteristics, :disability_status_car, :integer, limit: 4
  end
end
