class ChangeArrayColumnsToStringInPqHouseholds < ActiveRecord::Migration
  def change
    remove_column :pq_households, :other_members
    remove_column :pq_households, :children_ages

    add_column :pq_households, :other_members, :string
    add_column :pq_households, :children_ages, :string
  end
end
