class AddAttitudesToSpExperiments < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :attitudes, :text
  end
end
