class AddNumberOfTripsToPreSurveyTravelPatterns < ActiveRecord::Migration
  def change
    add_column :pre_survey_travel_patterns, :number_of_trips_car_driver, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_car_sharing, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_car_pax, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_bus, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_tram, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_metro, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_suburban_rail, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_rail, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_walk, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_bike, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_bike_sharing, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_taxi, :integer
    add_column :pre_survey_travel_patterns, :number_of_trips_taxi_app, :integer
  end
end
