class AddLabelToSpPlans < ActiveRecord::Migration
  def change
    add_column :sp_plans, :label, :string
  end
end
