class AddParticipatingToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :participating, :boolean
  end

  def self.down
    remove_column :users, :participating
  end
end
