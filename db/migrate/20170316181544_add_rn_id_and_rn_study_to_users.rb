class AddRnIdAndRnStudyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :rn_id, :string
    add_column :users, :rn_study, :string
  end
end
