# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210708160143) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addons", force: :cascade do |t|
    t.string   "label",      limit: 255
    t.string   "sub_label",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "hint",       limit: 255
    t.string   "addon_type", limit: 255
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.float    "lat"
    t.float    "lon"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",               default: 0
    t.integer  "attempts",               default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "queue",      limit: 255
  end

  add_index "delayed_jobs", ["priority", "run_at", "queue"], name: "delayed_jobs_priority", using: :btree

  create_table "exit_survey_resident_with_purchases", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "easiness_of_app"
    t.text     "elaboration_of_app_easiness"
    t.integer  "satisfaction_with_plans"
    t.text     "elaboration_of_plan_satisfaction"
    t.string   "reasons_for_choosing_plan",               limit: 255
    t.string   "reasons_for_not_choosing_plan_again",     limit: 255
    t.text     "elaboration_for_not_choosing_plan_again"
    t.integer  "choice_if_pay_as_you_go"
    t.text     "comments"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "exit_survey_resident_with_usages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "overall_experience"
    t.text     "elaboration_of_experience"
    t.integer  "easiness_of_app"
    t.text     "elaboration_of_app_easiness"
    t.integer  "satisfaction_with_plans"
    t.text     "elaboration_of_plan_satisfaction"
    t.string   "reasons_for_choosing_plan",        limit: 255
    t.integer  "recommend_to_friend"
    t.integer  "impact_analysis_1"
    t.integer  "impact_analysis_2"
    t.integer  "impact_analysis_3"
    t.integer  "impact_analysis_4"
    t.integer  "impact_analysis_5"
    t.integer  "impact_analysis_6"
    t.integer  "impact_analysis_7"
    t.integer  "impact_analysis_8"
    t.integer  "impact_analysis_9"
    t.integer  "impact_analysis_10"
    t.text     "comments"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "exit_survey_resident_without_purchases", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "easiness_of_app"
    t.text     "elaboration_of_app_easiness"
    t.string   "reasons_for_not_purchasing",  limit: 255
    t.text     "purchase_reason"
    t.text     "comments"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "generated_plans", force: :cascade do |t|
    t.integer  "stated_preference_experiment_id"
    t.string   "bases",                           limit: 255
    t.integer  "size"
    t.string   "toppings",                        limit: 255
    t.string   "addons",                          limit: 255
    t.float    "price"
    t.integer  "user_choice"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "pt_zones",                        limit: 255
    t.integer  "pt_discount"
    t.integer  "page_in_expt"
    t.float    "price_multiplier"
    t.text     "plan_data"
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type", limit: 255
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name",     limit: 255
    t.string   "action_name",         limit: 255
    t.string   "view_name",           limit: 255
    t.string   "request_hash",        limit: 255
    t.string   "ip_address",          limit: 255
    t.string   "session_hash",        limit: 255
    t.text     "message"
    t.text     "referrer"
    t.text     "params"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.integer  "sequence"
    t.text     "template"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "label",      limit: 255
    t.string   "sub_label",  limit: 255
  end

  create_table "plans_tasks", force: :cascade do |t|
    t.integer "plan_id"
    t.integer "task_id"
  end

  create_table "pq_household_vehicles", force: :cascade do |t|
    t.integer  "pq_mobility_private_id"
    t.string   "vehicle_label",           limit: 255
    t.float    "vehicle_tax_cost"
    t.float    "vehicle_fuel_cost"
    t.float    "monthly_parking_charges"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pq_households", force: :cascade do |t|
    t.integer  "total_income"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "other_members",      limit: 255
    t.string   "children_ages",      limit: 255
    t.integer  "pq_response_set_id"
  end

  create_table "pq_individual_demographics", force: :cascade do |t|
    t.integer  "age"
    t.string   "sex",                limit: 255
    t.string   "martial_status",     limit: 255
    t.string   "employment_status",  limit: 255
    t.string   "education",          limit: 255
    t.string   "ethnic_group",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
    t.string   "home_post_code",     limit: 255
    t.string   "work_post_code",     limit: 255
  end

  create_table "pq_mobility_attitudes", force: :cascade do |t|
    t.integer  "attitude_towards_innovative_products_1"
    t.integer  "attitude_towards_innovative_products_2"
    t.integer  "attitude_towards_innovative_products_3"
    t.integer  "attitude_towards_innovative_products_4"
    t.integer  "attitude_towards_innovative_products_5"
    t.integer  "attitude_towards_innovative_products_6"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
    t.integer  "attitude_towards_cars_1"
    t.integer  "attitude_towards_cars_2"
    t.integer  "attitude_towards_cars_3"
    t.integer  "attitude_towards_cars_4"
    t.integer  "attitude_towards_cars_5"
    t.integer  "attitude_towards_cars_6"
    t.integer  "attitude_towards_cars_7"
    t.integer  "attitude_towards_car_sharing_1"
    t.integer  "attitude_towards_car_sharing_2"
    t.integer  "attitude_towards_car_sharing_3"
    t.integer  "attitude_towards_car_sharing_4"
    t.integer  "attitude_towards_car_sharing_5"
    t.integer  "attitude_towards_car_sharing_6"
    t.integer  "attitude_towards_car_sharing_7"
    t.integer  "attitude_towards_car_sharing_8"
    t.integer  "attitude_towards_car_sharing_9"
  end

  create_table "pq_mobility_informations", force: :cascade do |t|
    t.integer  "smartphone_for_travel_usage"
    t.integer  "attitude_towards_journey_planner_1"
    t.integer  "attitude_towards_journey_planner_2"
    t.integer  "attitude_towards_journey_planner_3"
    t.integer  "attitude_towards_journey_planner_4"
    t.integer  "attitude_towards_journey_planner_5"
    t.string   "transport_modes_used",               limit: 255
    t.integer  "tool_none_usage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
  end

  create_table "pq_mobility_privates", force: :cascade do |t|
    t.string   "driving_licences",               limit: 255
    t.integer  "access_to_vehicle"
    t.integer  "number_of_cars"
    t.integer  "number_of_motorcycles"
    t.integer  "number_of_small_vans"
    t.integer  "number_of_other_vans"
    t.integer  "number_of_other_motor_vehicles"
    t.integer  "number_of_bicycles"
    t.string   "names_of_other_vehicles",        limit: 255
    t.integer  "vehicle_drivership"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
  end

  create_table "pq_public_transport_mobilities", force: :cascade do |t|
    t.string   "reduced_fare_passes",           limit: 255
    t.string   "travel_passes",                 limit: 255
    t.string   "other_pass_name",               limit: 255
    t.integer  "bus_pass_period"
    t.float    "bus_pass_cost"
    t.string   "travel_card_zones",             limit: 255
    t.integer  "travel_card_period"
    t.float    "travel_card_cost"
    t.string   "travel_card_number",            limit: 255
    t.string   "other_pass_modes",              limit: 255
    t.integer  "other_pass_period"
    t.float    "other_pass_cost"
    t.string   "other_pass_zones",              limit: 255
    t.string   "contactless_cards_used",        limit: 255
    t.integer  "disability_status_overall"
    t.integer  "disability_status_bus"
    t.integer  "disability_status_underground"
    t.integer  "disability_status_dlr"
    t.integer  "disability_status_tram"
    t.integer  "disability_status_rail"
    t.integer  "disability_status_black_cab"
    t.integer  "disability_status_uber"
    t.integer  "disability_status_cycling"
    t.string   "disability_status_walking",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
    t.string   "no_travelcard_zones",           limit: 255
  end

  create_table "pq_response_sets", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pq_shared_mobilities", force: :cascade do |t|
    t.integer  "aware_of_car_club"
    t.integer  "member_of_car_club"
    t.string   "car_clubs",                       limit: 255
    t.integer  "city_car_club_membership_plan"
    t.integer  "zipcar_membership_plan"
    t.integer  "drivenow_membership_plan"
    t.integer  "ecar_club_membership_plan"
    t.string   "other_membership_name",           limit: 255
    t.integer  "other_membership_plan"
    t.float    "other_membership_monthly_fees"
    t.float    "other_membership_yearly_fees"
    t.float    "car_club_usage_cost"
    t.integer  "used_santander_cycles"
    t.integer  "santander_access_scheme"
    t.integer  "frequency_of_black_cab_hailed"
    t.integer  "frequency_of_black_cab_app"
    t.integer  "frequency_of_uber"
    t.integer  "frequency_of_minicab"
    t.integer  "frequency_of_shared_taxi"
    t.float    "cost_of_black_cab_hailed"
    t.float    "cost_of_black_cab_app"
    t.float    "cost_of_uber"
    t.float    "cost_of_minicab"
    t.float    "cost_of_shared_taxi"
    t.integer  "taxicard_status"
    t.integer  "employer_pays_for_travel_status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pq_response_set_id"
    t.integer  "attitude_towards_uber_9"
    t.integer  "hourly_rental",                               default: 0
    t.integer  "daily_rental",                                default: 0
    t.integer  "monthly_rental_hours",                        default: 0
    t.integer  "monthly_rental_minutes",                      default: 0
  end

  create_table "pre_survey_household_characteristics", force: :cascade do |t|
    t.integer  "household_size"
    t.string   "other_members",            limit: 255
    t.string   "total_income",             limit: 255
    t.integer  "user_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "number_of_children_5",                 default: 0
    t.integer  "number_of_children_6_12",              default: 0
    t.integer  "number_of_children_13_17",             default: 0
  end

  create_table "pre_survey_journey_planner_characteristics", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "frequency_of_smartphone_usage"
    t.string   "transport_modes_used",                    limit: 255
    t.string   "journey_planners_used",                   limit: 255
    t.string   "most_important_information_from_planner", limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "decision_status"
    t.string   "journey_planner_usage",                   limit: 255
    t.integer  "journey_planner_trip_1"
    t.integer  "journey_planner_trip_2"
    t.integer  "journey_planner_trip_3"
    t.integer  "journey_planner_trip_4"
    t.integer  "journey_planner_trip_5"
    t.integer  "journey_planner_trip_6"
    t.integer  "journey_planner_trip_7"
    t.integer  "attitude_towards_journey_planner_1"
    t.integer  "attitude_towards_journey_planner_2"
    t.integer  "attitude_towards_journey_planner_3"
    t.integer  "travel_related_activities_1"
    t.integer  "travel_related_activities_2"
    t.integer  "travel_related_activities_3"
    t.integer  "travel_related_activities_4"
    t.integer  "travel_related_activities_5"
    t.integer  "travel_related_activities_6"
    t.integer  "travel_related_activities_7"
    t.integer  "travel_related_activities_8"
    t.integer  "attitude_towards_mobility_apps_1"
    t.integer  "attitude_towards_mobility_apps_2"
    t.integer  "attitude_towards_mobility_apps_3"
    t.integer  "attitude_towards_mobility_apps_4"
    t.integer  "attitude_towards_mobility_apps_5"
    t.integer  "attitude_towards_mobility_apps_6"
  end

  create_table "pre_survey_national_rail_characteristics", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "frequency_of_usage"
    t.string   "pass_period",        limit: 255
    t.float    "pass_cost"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "pass_membership"
  end

  create_table "pre_survey_private_mobility_characteristics", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "driving_licences",                     limit: 255
    t.integer  "number_of_cars"
    t.string   "frequency_of_car_usage",               limit: 255
    t.integer  "number_of_bicycles"
    t.string   "frequency_of_bicycle_usage",           limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "access_to_vehicle"
    t.integer  "main_driver"
    t.integer  "parking_scheme"
    t.integer  "attitude_towards_cars_of_owner_1"
    t.integer  "attitude_towards_cars_of_owner_2"
    t.integer  "attitude_towards_cars_of_owner_3"
    t.integer  "attitude_towards_cars_of_owner_4"
    t.integer  "attitude_towards_cars_of_owner_5"
    t.integer  "attitude_towards_cars_of_owner_6"
    t.integer  "attitude_towards_cars_of_non_owner_1"
    t.integer  "attitude_towards_cars_of_non_owner_2"
    t.integer  "attitude_towards_cars_of_non_owner_3"
    t.integer  "attitude_towards_cars_of_non_owner_4"
    t.integer  "attitude_towards_cars_of_non_owner_5"
    t.integer  "attitude_towards_cars_of_non_owner_6"
    t.integer  "vehicle_type"
    t.float    "vehicle_cost"
    t.integer  "attitude_towards_cars_of_owner_7"
    t.integer  "attitude_towards_cars_of_owner_8"
    t.integer  "attitude_towards_cars_of_owner_9"
    t.integer  "attitude_towards_cars_of_owner_10"
  end

  create_table "pre_survey_public_transport_characteristics", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "frequency_of_usage"
    t.string   "reasons_for_not_using",  limit: 255
    t.float    "pass_cost"
    t.string   "pass_period",            limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "pass_holder"
    t.string   "modes_with_valid_pass",  limit: 255
    t.integer  "fare_reduction"
    t.integer  "mode_from_home_to_stop"
  end

  create_table "pre_survey_shared_mobility_characteristics", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "aware_of_car_club"
    t.integer  "member_of_car_club"
    t.integer  "attitude_towards_car_sharing_owner_1"
    t.integer  "attitude_towards_car_sharing_owner_2"
    t.integer  "attitude_towards_car_sharing_owner_3"
    t.integer  "attitude_towards_car_sharing_owner_4"
    t.integer  "attitude_towards_car_sharing_owner_5"
    t.integer  "attitude_towards_car_sharing_owner_6"
    t.integer  "attitude_towards_car_sharing_owner_7"
    t.integer  "attitude_towards_car_sharing_owner_8"
    t.integer  "attitude_towards_car_sharing_non_owner_1"
    t.integer  "attitude_towards_car_sharing_non_owner_2"
    t.integer  "attitude_towards_car_sharing_non_owner_3"
    t.integer  "attitude_towards_car_sharing_non_owner_4"
    t.integer  "attitude_towards_car_sharing_non_owner_5"
    t.integer  "attitude_towards_car_sharing_non_owner_6"
    t.integer  "attitude_towards_car_sharing_non_owner_7"
    t.integer  "attitude_towards_car_sharing_non_owner_8"
    t.integer  "attitude_towards_car_sharing_non_owner_9"
    t.integer  "frequency_of_taxi_hailed"
    t.integer  "frequency_of_taxi_phone"
    t.integer  "frequency_of_taxi_app"
    t.integer  "frequency_of_taxi_web"
    t.integer  "attitude_towards_taxi_1"
    t.integer  "attitude_towards_taxi_2"
    t.integer  "attitude_towards_taxi_3"
    t.integer  "attitude_towards_taxi_4"
    t.integer  "attitude_towards_taxi_5"
    t.integer  "attitude_towards_taxi_6"
    t.integer  "attitude_towards_taxi_7"
    t.integer  "attitude_towards_taxi_8"
    t.integer  "attitude_towards_taxi_9"
    t.integer  "attitude_towards_taxi_10"
    t.integer  "frequency_of_car_sharing"
    t.float    "cost_of_shared_vehicle"
    t.integer  "aware_of_ride_sharing"
    t.integer  "aware_of_bike_sharing"
    t.integer  "member_of_bike_sharing"
    t.integer  "frequency_of_bike_sharing"
    t.integer  "number_of_hours_of_car_sharing"
  end

  create_table "pre_survey_socio_economic_characteristics", force: :cascade do |t|
    t.integer  "age"
    t.integer  "user_id"
    t.string   "sex",                                 limit: 255
    t.string   "marital_status",                      limit: 255
    t.string   "education",                           limit: 255
    t.string   "employment_status",                   limit: 255
    t.string   "employment_flexibility",              limit: 255
    t.float    "duration_from_home_to_work"
    t.string   "transport_mode_for_grocery_shopping", limit: 255
    t.string   "transport_mode_for_leisure",          limit: 255
    t.string   "disability_status",                   limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "disability_status_bus"
    t.integer  "disability_status_underground"
    t.integer  "disability_status_tram"
    t.integer  "disability_status_suburban_rail"
    t.integer  "disability_status_rail"
    t.integer  "disability_status_taxi"
    t.integer  "disability_status_cycling"
    t.integer  "disability_status_walking"
    t.string   "transport_mode_for_work",             limit: 255
    t.integer  "ethnic_group"
    t.text     "home_location"
    t.text     "work_locations"
    t.text     "shop_locations"
    t.text     "leisure_locations"
    t.integer  "disability_status_metro"
    t.integer  "disability_status_car"
    t.string   "residential_type",                    limit: 255
  end

  create_table "pre_survey_travel_patterns", force: :cascade do |t|
    t.text     "home_location"
    t.text     "work_locations"
    t.text     "shop_locations"
    t.text     "leisure_locations"
    t.text     "map_data"
    t.integer  "transport_mode_for_work"
    t.float    "duration_from_home_to_work"
    t.string   "important_factors_for_work_mode",    limit: 255
    t.integer  "transport_mode_for_grocery"
    t.float    "duration_from_home_to_grocery"
    t.string   "important_factors_for_grocery_mode", limit: 255
    t.integer  "transport_mode_for_leisure"
    t.float    "duration_from_home_to_leisure"
    t.string   "important_factors_for_leisure_mode", limit: 255
    t.string   "activities_while_travelling",        limit: 255
    t.integer  "attitude_towards_daily_mobility_1"
    t.integer  "attitude_towards_daily_mobility_2"
    t.integer  "attitude_towards_daily_mobility_3"
    t.integer  "attitude_towards_daily_mobility_4"
    t.integer  "attitude_towards_daily_mobility_5"
    t.integer  "attitude_towards_daily_mobility_6"
    t.integer  "attitude_towards_daily_mobility_7"
    t.integer  "attitude_towards_combining_modes_1"
    t.integer  "attitude_towards_combining_modes_2"
    t.integer  "attitude_towards_combining_modes_3"
    t.integer  "attitude_towards_combining_modes_4"
    t.integer  "attitude_towards_combining_modes_5"
    t.integer  "private_vehicle"
    t.integer  "user_id"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "number_of_trips_car_driver"
    t.integer  "number_of_trips_car_sharing"
    t.integer  "number_of_trips_car_pax"
    t.integer  "number_of_trips_bus"
    t.integer  "number_of_trips_tram"
    t.integer  "number_of_trips_metro"
    t.integer  "number_of_trips_suburban_rail"
    t.integer  "number_of_trips_rail"
    t.integer  "number_of_trips_walk"
    t.integer  "number_of_trips_bike"
    t.integer  "number_of_trips_bike_sharing"
    t.integer  "number_of_trips_taxi"
    t.integer  "number_of_trips_taxi_app"
    t.text     "home_postcode"
    t.text     "home_address"
    t.text     "work_postcodes"
    t.text     "work_addresses"
    t.text     "shop_postcodes"
    t.text     "shop_addresses"
    t.text     "leisure_postcodes"
    t.text     "leisure_addresses"
  end

  create_table "pre_surveys", force: :cascade do |t|
    t.text     "socio_demographic"
    t.text     "travel_information"
    t.text     "travel_habits"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.integer  "time_for_completion"
  end

  create_table "seeded_generated_plans", force: :cascade do |t|
    t.integer  "seeded_stated_preference_experiment_id"
    t.integer  "page_in_expt"
    t.text     "plan_data"
    t.string   "plan_type",                              limit: 255
    t.boolean  "selected",                                           default: false
    t.text     "other_answers"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
  end

  create_table "seeded_stated_preference_experiments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "seed_data"
    t.boolean  "completed",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255, null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "sp_attitudes_and_impacts", force: :cascade do |t|
    t.integer  "seeded_stated_preference_experiment_id"
    t.integer  "attitude_towards_car_1"
    t.integer  "attitude_towards_car_2"
    t.integer  "attitude_towards_car_3"
    t.integer  "attitude_towards_car_4"
    t.integer  "most_likely_substitute_1"
    t.integer  "most_likely_substitute_2"
    t.integer  "most_likely_substitute_3"
    t.integer  "attitude_towards_car_ownership_1"
    t.integer  "attitude_towards_car_ownership_2"
    t.integer  "impact_on_public_transport_use_1"
    t.integer  "impact_on_public_transport_use_2"
    t.integer  "impact_on_public_transport_use_3"
    t.integer  "impact_on_bike_use_1"
    t.integer  "impact_on_bike_use_2"
    t.integer  "impact_on_bike_use_3"
    t.integer  "attitude_towards_maas_1"
    t.integer  "attitude_towards_maas_2"
    t.integer  "attitude_towards_maas_3"
    t.integer  "attitude_towards_maas_4"
    t.integer  "attitude_towards_maas_5"
    t.integer  "attitude_towards_maas_6"
    t.integer  "attitude_towards_maas_7"
    t.string   "most_likely_substitutes",                limit: 255
    t.string   "impacts_on_public_transport_use",        limit: 255
    t.string   "impacts_on_bike_use",                    limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "attitude_towards_maas_8"
    t.integer  "attitude_towards_maas_9"
    t.integer  "attitude_towards_maas_10"
  end

  create_table "sp_experiments", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "completed",        default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "plans"
    t.text     "responses"
    t.text     "flexible_plan"
    t.text     "attitudes"
    t.text     "completion_times"
    t.string   "locale"
  end

  create_table "sp_plans", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "plan_id"
    t.integer  "level"
    t.integer  "sp_plan_id"
    t.float    "price"
    t.boolean  "selected"
    t.text     "other_data"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "sp_experiment_id"
    t.string   "label",            limit: 255
    t.string   "task_label",       limit: 255
    t.text     "plan_data"
    t.boolean  "flexible",                     default: false
    t.text     "variable_data"
    t.integer  "sequence"
  end

  create_table "stated_preference_experiments", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "completed",                              default: false
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "seeded_stated_preference_experiment_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "label",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",               limit: 255
    t.string   "crypted_password",    limit: 255
    t.string   "password_salt",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "household_id"
    t.string   "name",                limit: 255
    t.integer  "role_id"
    t.datetime "last_login_at"
    t.datetime "current_login_at"
    t.datetime "current_login_ip"
    t.boolean  "active"
    t.string   "persistence_token",   limit: 255
    t.string   "perishable_token",    limit: 255
    t.boolean  "complete",                        default: false
    t.string   "gender",              limit: 255
    t.string   "age",                 limit: 255
    t.string   "education",           limit: 255
    t.string   "relationship",        limit: 255
    t.string   "registration_source", limit: 255
    t.boolean  "participating"
    t.integer  "feedback_complete"
    t.boolean  "consent",                         default: false
    t.datetime "ts_last_upload"
    t.boolean  "recalculate",                     default: false
    t.datetime "app_turned_off"
    t.boolean  "tutorial",                        default: false
    t.datetime "finished_ad"
    t.integer  "totalLogTime"
    t.string   "time_zone",           limit: 255
    t.integer  "points",                          default: 0
    t.datetime "last_pushed"
    t.string   "imei",                limit: 255
    t.string   "device_model",        limit: 255
    t.string   "device_os_version",   limit: 255
    t.string   "device_identifier",   limit: 255
    t.string   "single_access_token", limit: 255,                 null: false
    t.string   "username",            limit: 255
    t.string   "study",               limit: 255
    t.string   "panelist_secure_id",  limit: 255
    t.string   "wsp_email",           limit: 255
    t.string   "rn_id",               limit: 255
    t.string   "rn_study",            limit: 255
    t.string   "browser",             limit: 255
    t.boolean  "screenout",                       default: false
    t.integer  "city_id"
    t.string   "umove_id",            limit: 255
    t.string   "user_type",           limit: 255
    t.string   "device",              limit: 255
    t.string   "ip_address",          limit: 255
    t.string   "survey_email_start",  limit: 255
    t.string   "survey_email_finish", limit: 255
    t.float    "rn_status"
    t.integer  "rn_age"
    t.integer  "rn_sex"
    t.integer  "rn_licence"
  end

end
