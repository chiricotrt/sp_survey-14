Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  APP_DOMAIN = "localhost:3000"

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = { :host => APP_DOMAIN }

  config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.perform_deliveries = true
  config.action_mailer.smtp_settings = {
    :enable_starttls_auto => true,
    :port => 25,
    :address => "smtp.mandrillapp.com", #smtp server address here
    :domain => APP_DOMAIN,
    :authentication => :login,
    :user_name => "UCL", #username of the email address, such as 'greenhomes'
    :password => "zVkGe1CEtAcacVHwUJ7rhw" #password of the email account
  }

  # config.action_mailer.delivery_method = :smtp
  # config.action_mailer.perform_deliveries = true
  # config.action_mailer.raise_delivery_errors = true
  # config.action_mailer.smtp_settings = {
  #   enable_starttls_auto: true,
  #   port: 25,
  #   address: "smtp.mandrillapp.com", #smtp server address here
  #   authentication: :login,
  #   user_name: "UCL", #username of the email address, such as 'greenhomes'
  #   password: "zVkGe1CEtAcacVHwUJ7rhw" #password of the email account
  # }
  # config.action_mailer.smtp_settings = {
  #   :address              => "smtp.gmail.com",
  #   :port                 => 465,
  #   :domain               => "gmail.com",
  #   :user_name            => "londonfms@gmail.com",
  #   :password             => "UCLfms2O!5",
  #   :authentication       => :plain,
  #   :enable_starttls_auto => true
  # }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Do not compress assets
  config.assets.compress = false

  config.assets.compile = true
  config.assets.check_precompiled_asset = false

  # Expands the lines which load the assets
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # For console to be accessible in VMs
  config.web_console.whitelisted_ips = '192.168.0.0/16'

  Bullet.enable = true
  Bullet.console = true
  Bullet.rails_logger = true
  Bullet.bullet_logger = true
end
