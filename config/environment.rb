# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Rails.application.initialize!

# support for locale fallbacks (beyond just "translation missing" messages)
require "i18n/backend/fallbacks"
I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
