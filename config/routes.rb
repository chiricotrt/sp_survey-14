Rails.application.routes.draw do
  get 'sections/show'

  resources :pre_survey_travel_patterns
  resources :pre_surveys do
    collection do
      get :new_tourist
      get :new_resident
    end
  end

  # SP Experiment
  resources :sp_experiments do
    resource :activity_choices
    resource :day_to_day_learnings
    resources :day_choices
  end

  # Pre-survey
  resources :pre_survey_journey_planner_characteristics
  resources :pre_survey_private_mobility_characteristics
  # resources :pre_survey_household_characteristics
  resources :pre_survey_socio_economic_characteristics
  match 'pre_questionnaire' => 'pre_survey_socio_economic_characteristics#new', :via => [:post, :get]
  match 'finish_presurvey' => 'pre_survey_journey_planner_characteristics#finish_presurvey', :via => [:post, :get]
  
  resources :tests

  # Users section
  resource :users, only: [:new, :create, :update] do
    patch :update_password, on: :member
    post :submitIgnorePopup
    post :acknowledge_warning
    get :dismiss_timezone_warnings
    get :update_wsp_email
    get :new_wsp
    get :sync_umove
  end

  resources :users do
    member do
      get :screenout
    end
  end

  get 'users/:study/new' => 'users#new', as: :new_users_for_study
  post 'users/:study' => 'users#create', as: :users_for_study

  resource :user_session, only: [:create, :destroy] do
    post :change_timezone
  end

  namespace :activations do
    get :new
    get ":activation_code", action: :create
    post :resend, as: :resend
  end

  resources :password_resets, only: [:new, :create, :edit, :update]

  resources :sections do
    get :next_section, on: :collection
  end

  # get "/wsp_register" => "users#new_wsp"

  # Static pages and AD section

  namespace :pages do
    get :home
    get :new_home
    get :thankyou
    get :newuser
    get :calendar
    post :calendar
    get :feedback_feedback
    get :consent
    get :contacts
    get :faqs
    get :dashboard
    get :support
    post :support_submit
    patch :accept_consent
    patch :deactivate_user
    # root to: :index # BEFORE
    get :info
    get :overview
    get :image_test
    root action: :index
    get :wsp
    get :disclaimer
    get :statistics
    get :summarise
    get :backoffice_container
    get :backoffice
    get :tab_1
    get :tab_2
    # get :tab_3
    get :tab_3_1
    get :tab_3_2
    get :tab_3_3
    get :gateway
    post :survey
  end

  match "/delayed_job" => DelayedJobWeb, anchor: false, via: [:get, :post]#, constraints: AdminConstraint.new

  root :to => 'pages#survey'
end
