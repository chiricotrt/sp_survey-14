# namespace :deploy do
#   desc 'Runs rake db:seed to populate the database'
#   task :seed do
#     run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
#   end
#   # task :seed => [:set_rails_env] do
#   #   on primary fetch(:migration_role) do
#   #     within release_path do
#   #       with rails_env: fetch(:rails_env) do
#   #         execute :rake, "db:seed"
#   #       end
#   #     end
#   #   end
#   # end
# end

# namespace :custom do
#   desc 'run some rake db task with params'
#   task :run_db_task, :param do
#     on roles(:app) do
#       within "#{current_path}" do
#         with rails_env: "#{fetch(:stage)}" do
#           execute :rake, args[:param]
#         end
#       end
#     end
#   end
# end


namespace :deploy do
  desc 'Runs rake db:seed to populate the database'
  task :seed do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "db:seed"
        end
      end
    end
  end
end
