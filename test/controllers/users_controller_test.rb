require 'test_helper'

describe UsersController do
	fixtures(:users)

  describe "GET :new" do
    before do
      Backoffice::Study.create(title: 'test-title', code: 'test-study')
      get :new, study: 'test-study'
    end

    it "renders :new" do
    	assigns(:user).wont_be_nil
      must_respond_with :success
      must_render_template "users/new"
    end
  end

  describe "POST :create" do
  	describe "with valid attributes" do
	  	before do
  			@user_count = User.count
	  		post :create, user: { name: "example", email: "example@example.com", username: "example", password: "passwd", password_confirmation: "passwd", time_zone: "Singapore" }
	  	end

	  	it "creates a new user and redirects to root path" do
	  		User.count.must_equal @user_count + 1
	  		must_redirect_to root_path
	  	end

	  	it "delivers a email with activation instructions" do
		  	ActionMailer::Base.deliveries.last.to.must_equal ["example@example.com"]
		  	ActionMailer::Base.deliveries.last.subject.must_equal "Activation Instructions"
	  	end
  	end

  	describe "with invalid attributes (missing fields)" do
  		before do
  			post :create, user: { name: "example" }
  		end

  		it "re-renders the :new template with errors" do
  			assigns(:user).errors.wont_be_empty
  			must_render_template :new
  		end
  	end

    describe "with invalid attributes (duplicated username)" do
      before do
        post :create, user: { name: "example", email: "example@example.com", username: "test", password: "passwd", password_confirmation: "passwd", time_zone: "Singapore" }
      end

      it "re-renders the :new template with errors" do
        assigns(:user).errors.wont_be_empty
        must_render_template :new
      end
    end

    describe "with missing email field (human)" do
      before do
        @login_user_hash = { name: "example", username: "test", password: "passwd", password_confirmation: "passwd", time_zone: "Singapore" }
        post :create, user: @login_user_hash
      end

      it "re-renders the :new template with errors" do
        assigns(:user).errors.wont_be_empty
        must_render_template :new
      end
    end
  end

  describe "PATCH :update_password" do
  	before do
  		@user = users(:one)
    	ApplicationController.any_instance.stubs(:current_user).returns(@user)
    	@old_pwd = @user.crypted_password
  	end

  	describe "with password matching password_confirmation" do
  		before do
  			patch :update_password, user: {current_password: "1234", password: "loremipsum", password_confirmation: "loremipsum"}
  		end

	  	it "updates the user password and redirects to dashboard" do
	  		assert_not_equal(@user.reload.crypted_password, @old_pwd)
	  		must_redirect_to pages_dashboard_path
	  	end
  	end

  	describe "with password not matching password_confirmation" do
  		before do
  			patch :update_password, user: {current_password: "1234", password: "loremipsum", password_confirmation: "dolorsitamet"}
  		end

		   	it "does not update the password redirects to dashboard" do
		  		assert_equal(@user.reload.crypted_password, @old_pwd)
		  		must_redirect_to pages_dashboard_path
		  	end
  	end
  end
end
