# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  email               :string(255)
#  crypted_password    :string(255)
#  password_salt       :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  household_id        :integer
#  name                :string(255)
#  last_login_at       :datetime
#  current_login_at    :datetime
#  current_login_ip    :datetime
#  active              :boolean
#  persistence_token   :string(255)
#  perishable_token    :string(255)
#  complete            :boolean          default(FALSE)
#  gender              :string(255)
#  age                 :string(255)
#  education           :string(255)
#  relationship        :string(255)
#  registration_source :string(255)
#  participating       :boolean
#  feedback_complete   :integer
#  consent             :boolean          default(FALSE)
#  ts_last_upload      :datetime
#  recalculate         :boolean          default(FALSE)
#  app_turned_off      :datetime
#  tutorial            :boolean          default(FALSE)
#  finished_ad         :datetime
#  totalLogTime        :integer
#  number_wbq          :integer          default(0)
#  time_zone           :string(255)
#  points              :integer          default(0)
#

class User < ActiveRecord::Base
  # we need this line due to existing bug since Rails 4.0
  self.skip_time_zone_conversion_for_attributes = [:current_login_ip]

	acts_as_authentic do |c|
		c.merge_validates_uniqueness_of_login_field_options({case_sensitive: true, allow_blank: true, allow_nil: true, :unless => :guest?})
    c.merge_validates_format_of_login_field_options({allow_blank: true, allow_nil: true})
    c.merge_validates_length_of_login_field_options({allow_blank: true, allow_nil: true})

    # email is needed for human registration
    c.merge_validates_format_of_email_field_options({allow_blank: false, allow_nil: false, on: :human, :unless => :guest?})
    c.validate :email_or_username_must_exist, unless: Proc.new{ |u| u.validation_context == :human }

    c.transition_from_crypto_providers = Authlogic::CryptoProviders::Sha512
    c.crypto_provider = Authlogic::CryptoProviders::SCrypt
    c.perishable_token_valid_for 24.hours
  end

  validate :time_zone_check

  # PRE-SURVEY
  has_one :pre_survey_socio_economic_characteristic
  has_one :pre_survey_household_characteristic
  has_one :pre_survey_private_mobility_characteristic
  has_one :pre_survey_public_transport_characteristic
  has_one :pre_survey_national_rail_characteristic
  has_one :pre_survey_shared_mobility_characteristic
  has_one :pre_survey_journey_planner_characteristic
  has_one :pre_survey_travel_pattern

  # PRE-SURVEY (for non-app users)
  has_one :pre_survey

  # STATED-PREFERENCE EXPERIMENT / POST-SURVEY
  has_one :sp_experiment
  has_many :sp_plans, through: :sp_experiment
  # has_many :stated_preference_experiments
  # has_many :generated_plans, through: :stated_preference_experiments

  # EXIT SURVEY
  has_one :exit_survey_resident_without_purchase
  has_one :exit_survey_resident_with_purchase
  has_one :exit_survey_resident_with_usage

  has_many :seeded_stated_preference_experiments
  has_many :seeded_generated_plans, through: :seeded_stated_preference_experiments
  belongs_to :city

  attr_accessible :name, :password, :password_confirmation, :email, :username, :imei
  attr_accessible :household_id, :active, :complete, :registration_source, :study
  attr_accessible :participating, :gender, :age, :time_zone, :feedback_complete
  attr_accessible :device_os_version, :device_model, :device_identifier
  attr_accessible :ezlink_data_attributes, :last_login_at, :current_login_at
  attr_accessible :panelist_secure_id
  attr_accessible :wsp_email

  attr_accessible :rn_id, :rn_study, :rn_status # Research Now
  attr_accessible :rn_age, :rn_sex, :rn_licence # Research Now

  attr_accessible :browser, :device, :ip_address
  attr_accessible :umove_id # UMove
  attr_accessible :user_type
  attr_accessible :survey_email_start, :survey_email_finish

  DESKTOP = 1
  MOBILE = 2
  TABLET = 3

  # Return pq-response-set id (create if not present)
  def pq_response_set_id
    return pq_response_set.id if pq_response_set
    return PqResponseSet.create(user_id: id).id
  end

  def self.find_by_username_or_email(handle)
    User.find_by_username(handle) || User.find_by_email(handle)
  end

	def activate!
		self.active = true
    # self.consent = true
		save
	end

  def consent!
    self.consent = true
    save
  end

  # + Research Now + #
  def complete!
    self.rn_status = 1
    save
  end

  def screenout!()
    # self.screenout = true
    self.rn_status = 2
    save
  end

  # Overquota reason
  # 3.1: licence
  # 3.2: gender
  # 3.3: age
  def overquota!(quota_value)
    self.rn_status = quota_value
    save
  end

  def self.completed_users_man
    where("rn_id is not null and rn_status = 1")
  end

  # 30%  18-30 years old -> 120
  # 35%  31-45 years old -> 140
  # 30%  46-60 years old -> 120
  # 5%   More than 61 years old -> 20
  MANCHESTER_QUOTA = 400
  def self.check_quota_age(age)
    case age
    when 18..30
      (completed_users_man.where("rn_age >= 18 and rn_age <= 30").length <= MANCHESTER_QUOTA * 0.30)
    when 31..45
      (completed_users_man.where("rn_age >= 31 and rn_age <= 45").length <= MANCHESTER_QUOTA * 0.35)
    when 46..60
      (completed_users_man.where("rn_age >= 46 and rn_age <= 60").length <= MANCHESTER_QUOTA * 0.30)
    else
      (completed_users_man.where("rn_age > 60").length <= MANCHESTER_QUOTA * 0.05)
    end
  end

  # 50/50 gender spilt -> 200
  def self.check_quota_sex(sex)
    case sex
    when 1
      (completed_users_man.where("rn_sex = 1").length <= MANCHESTER_QUOTA * 0.50)
    when 2
      (completed_users_man.where("rn_sex = 2").length <= MANCHESTER_QUOTA * 0.50)
    end
  end

  # At least 60% to have a driving license -> 240 -> no-licence at most 160
  def self.check_quota_licence(licence)
    if licence == 0
      completed_users_man.where("rn_licence = 0").length <= MANCHESTER_QUOTA * 0.40
    else
      true
    end
  end

  # - Research Now - #

	def send_activation_instructions!
		reset_perishable_token!
		Notifier.activation_instructions(self).deliver_now
	end

	def send_activation_confirmation!
		reset_perishable_token!
		Notifier.activation_confirmation(self).deliver_now
	end

	def send_password_reset_instructions!
		reset_perishable_token!
		Notifier.password_reset_instructions(self).deliver_now
	end

	def send_support_data!(data)
		Notifier.support_data(self, data).deliver_now
	end

	def send_support_notification!
		Notifier.support_notification(self).deliver_now
 	end

	def claim_incentive!(user_confirmed_data, days_data, days_validated)
		Notifier.request_incentive(self, user_confirmed_data, days_data, days_validated).deliver_now
	end

	def send_reminder_not_using!
		Notifier.reminder_not_using(self).deliver_now
	end

	def send_reminder_not_using_after_start_using!
		counters = self.validatedDays(:counters)
		Notifier.reminder_not_using_after_start_using(self, counters[0], counters[1]).deliver_now
	end

	def send_reminder_not_validating!
		Notifier.reminder_not_validating(self).deliver_now
	end

	def send_reminder_missing_days!
		Notifier.reminder_missing_days(self).deliver_now
	end

	def send_reminder_missing_validation!
		Notifier.reminder_missing_validation(self).deliver_now
	end

	def send_ready_for_exit!
		Notifier.ready_for_exit(self).deliver_now
	end

	def send_welcoming_email!
		Notifier.welcoming_email(self).deliver_now
	end

	def send_mobile_app_off!
		Notifier.mobile_app_off(self).deliver_now
	end

	# def is_household?
	# 	has_role? "Head of Household"
	# end

	# def household_size
	# 	return User.where(household_id: household_id).size
	# end

	# copied from old user model  - zf 2012-03-01
	def send_password_reset_instructions!
		reset_perishable_token!
		Notifier.password_reset_instructions(self).deliver_now
	end

	def generate_token
		return encrypt("#{email}--#{2.weeks.from_now.utc}")
	end

	# Encrypts some data with the salt.
	def self.encrypt(password, salt)
		Digest::SHA1.hexdigest("--#{salt}--#{password}--")
	end

	# Encrypts the password with the user salt
	def encrypt(password)
		self.class.encrypt(password, password_salt)
	end

	def authtoken
		single_access_token
	end

	def finished
		if !self.finished_ad.nil?
			return [true, nil]
		end

		num_validated, num_days = self.validatedDays(:counters)
		if num_validated >= MobilityRecord::Generator::NUMBER_OF_DAYS_FOR_ANALYSIS #5
            #if num_validated >=5 && num_days >=14
			if self.finished_ad.nil?
				self.finished_ad = Time.now
				save
			end
			return [true, nil]
		else
			#if num_days < 14
			#	return [false, 'num_days']
			#else
			return [false, 'num_validated']
			#end
		end
	end

  def has_role?(value)
    false
  end

	def is_admin?
  	has_role?(:admin)
  end

	def validatedDays(ansformat="", starttime=nil, endtime=nil)
		if starttime.nil? and endtime.nil?
			daysInTimezone = self.daysInTimezone
		else
      daysInTimezone = self.daysInTimezone(starttime: starttime, endtime: endtime)
		end

    days = daysInTimezone.keys

		#case :listDays
		validated_list = []
		#case :counters
		no_days_validated = 0
		no_days_data = 0
		#case :listBinaryDays
		validated_binary = []
		avg_count = 0

		daysInTimezone.each do |day,validated|
      if (validated)
				validated_list << day
			end
			validated_binary << validated
    end

    if starttime.present? and endtime.present?
      stops_list = self.stop.where("starttime>= ? and endtime<=? and deleted = ?",starttime,endtime,false).order(:starttime)
    else
      stops_list = self.stop.where("deleted = ?",false).order(:starttime)
    end

    stopsByDay = Hash.new{|h,k| h[k] = [] }
    stops_list.each do |stop|
      (stop.starttime.to_date..stop.endtime.to_date).each do |day|
        date_key = day.strftime("%-d-%-m-%Y")
        stopsByDay[date_key] << stop
      end
    end

    stopsByDay.keys.each do |day|
      if !stopsByDay[day].nil?
        avg = (stopsByDay[day].last.starttime - stopsByDay[day].first.endtime).to_i
      end

      if (not avg.nil? and (avg/60/60)>=10)
        avg_count += 1
      end
    end


    no_days_validated = validated_list.length
		no_days_data = days.length

		case ansformat
		when :listDays
			return validated_list
		when :counters
			return no_days_validated, no_days_data
		when :countersWithAvg
			return no_days_validated, no_days_data, avg_count
		when :listBinaryDays
			return validated_binary, days
		when :userClassifier
			return stopsByDay
		else
			puts "You gave me #{ansformat} -- I have no idea what to do with that."
			return nil
		end
	end

	def myCalendar(mnthyear=nil)
		if mnthyear.nil?
			day_list = UserProperty.where(user_id: id, name: DAY_TO_VALIDATE).select(:value)
		else
			starttime = DateTime.parse(mnthyear)
			endtime = starttime + 1.month
			day_list = UserProperty.where(user_id: id, name: DAY_TO_VALIDATE).where("STR_TO_DATE(value, '%Y-%m-%d') >= ? and STR_TO_DATE(value, '%Y-%m-%d') < ?", starttime, endtime).select(:value)
		end
			validated_binary_list = {}
			day_list.each do |good_day|
			date = DateTime.parse(good_day.value)
			validated_binary_list[good_day.value] = isDayValidated(date)
		end
		return validated_binary_list
	end

	def isDayToValidate(day)
		up = UserProperty.where(user_id: id, name: DAY_TO_VALIDATE, value: day).select(:value).first
		return (not up.nil?)
	end

	def isDayValidated(selectedData, returnAvg=false)
		stops = self.stop.where("endtime >= ? and starttime < ? and deleted = ?", selectedData, selectedData + 1.day, false).order(:starttime).select("validated, deleted, starttime, endtime")
		return areStopsValidated(stops, returnAvg)
	end

	def areStopsValidated(stops, returnAvg)
		if stops.nil? or stops.length == 0
			return false
		end
		validated = true
		stops.each do |s_is_valid|
			if ( s_is_valid == stops.first )
				if !((s_is_valid.validated & 0x1) > 0)
					validated = false
					break
				end
			elsif !(s_is_valid.validated==3) && !s_is_valid.deleted
				validated = false
				break
			end
		end

		if (returnAvg)
			return validated, (stops.last.starttime - stops.first.endtime).to_i
		else
			return validated
		end
	end

	def dailyStops(selectedData)
		stops = self.stop.includes(:mode, :stop_trace_segment, :stops_user_activity => :user_activity).where("endtime >= ? and starttime < ? and deleted = false", selectedData, selectedData+1.day).order("starttime")
		return stops
	end

	def missingUserVoice?
		property = UserProperty.where("user_id = ? and name like ?", id, SCHEDULE_PROMPT).first
		property.nil? ? (return false) : (return (property.value == READY_SCHEDULE.to_s) )
	end

	def suggestedDays
		UserProperty.where("user_id = ? and name like ?", id, SUGGESTED_DAY)
	end

	def daysToValidate
		UserProperty.where("user_id = ? and name like ?", id, DAY_TO_VALIDATE)
	end

  def isSuggested?(day)
    UserProperty.where("user_id = ? and name like ? and value = ?", id, SUGGESTED_DAY, day).count > 0 ? true : false
  end

  def shouldPromptWhat?
    property = UserProperty.where("user_id = ? and name like ?", id, SCHEDULE_PROMPT).first
    if property.nil?
      property = UserProperty.new(:user_id => self.id,
                                  :name => SCHEDULE_PROMPT,
                                  :value => IGNORED_WELCOME_MSG)
      property.save
      return "feedback"
    else
      case property.value
      when NEVER_VALIDATED.to_s
        return "welcome"
      when IGNORED_WELCOME_MSG.to_s
        return "feedback"
      else
        return ""
      end
    end
  end

  def updatePromptProperty(lastprompt)
    property = UserProperty.where("user_id = ? and name like ?", id, SCHEDULE_PROMPT).first
    newValue = PROMPT_NEXT_VALUE[lastprompt]

    if property.nil?
      property = UserProperty.new(:user_id => self.id,
                                  :name => SCHEDULE_PROMPT,
                                  :value => newValue)
    else
      if property.value.to_i < newValue
        property.value = newValue
      end
    end
    property.save
  end

  def completedPhoneTutorial
    property = UserProperty.where("user_id = ? and name like ?", id, SCHEDULE_PROMPT).first
    if !property.nil?
      property.value = SESSION_COMPLETED
    else
      property = UserProperty.new(:user_id => self.id,
                                  :name => SCHEDULE_PROMPT,
                                  :value => SESSION_COMPLETED)
    end
    property.save
  end

  def presurvey_response
    # @surveys = Survey.where(:access_code => Survey::PRESURVEY_NAME).order("survey_version DESC")
    @surveys = Survey.where(:access_code => "ucl-presurvey").order("survey_version DESC")
    @response_set = ResponseSet.where(user_id: self.id, survey_id: @surveys.map(&:id)).order("created_at DESC").first
  end

  def completedPresurvey
    if pre_survey_socio_economic_characteristic and pre_survey_household_characteristic and
      pre_survey_private_mobility_characteristic and pre_survey_public_transport_characteristic and
      pre_survey_shared_mobility_characteristic and pre_survey_journey_planner_characteristic
      true
    else
      false
    end
  end

  def presurvey_completed?
    if pre_survey_socio_economic_characteristic and pre_survey_household_characteristic and
      pre_survey_private_mobility_characteristic and pre_survey_public_transport_characteristic and
      pre_survey_shared_mobility_characteristic and pre_survey_journey_planner_characteristic
      true
    else
      false
    end
  end

  def tourist?
    (user_type == "tourist")
  end

  def completedPostSurvey
    false
    # sp_experiment.pluck(:completed).uniq.include?(true)
  end

  def downloaded_app?
    # true
    not gps_datas.empty?
  end

  def promptWarning?
    warning = UserProperty.where("user_id = ? and name like ?", id, WARNING_PROMPT).first
    #warning = self.user_property.find_by_name(WARNING_PROMPT)
    if warning.nil?
      user_property = UserProperty.new(:user_id => id, :name => WARNING_PROMPT, :value => '0')
      user_property.save
      return true
    else
      return (warning.value == '0' ? true : false)
    end
  end

  def acknowledgeWarning
    warning = UserProperty.where("user_id = ? and name like ?", id, WARNING_PROMPT).first
    warning.value = '1'
    warning.save
  end

  def getTravelSegments(date_str)
    travelSegments = []
    date_to_search = date_str.in_time_zone(self.time_zone).at_midnight

    #day_stops = self.stop.where('endtime >= ? and starttime < ?', date_to_search, date_to_search+1.day).order(:starttime).select("starttime, endtime")
    day_stops = self.stop.where('IFNULL(replaced_endtime, endtime) >= ? and IFNULL(replaced_starttime, starttime) < ? and source_id != ?', date_to_search, date_to_search+1.day, 2).select("IFNULL(replaced_starttime, starttime) as starttime1, IFNULL(replaced_endtime, endtime) as endtime1").order("starttime1")

    if day_stops.nil? or day_stops.empty?
      return day_stops, travelSegments
    end

    tmp_starttime = day_stops[0].starttime1.in_time_zone(self.time_zone)
    if tmp_starttime > date_to_search
      travelSegments << {'starttime' => date_to_search, 'endtime' => tmp_starttime }
    end
    index = 1
    day_stops.each do |current|

      tmp_endtime = current.endtime1.in_time_zone(self.time_zone)

      if day_stops[index].nil?
        #verify if stop ends after 23.59
        day_at_end = date_to_search + 1.day - 1.minute

        if tmp_endtime <= day_at_end
          travelSegments << {'starttime' => tmp_endtime, 'endtime' => day_at_end }
        end
      else
        tmp_starttime = day_stops[index].starttime1.in_time_zone(self.time_zone)
        travelSegments << {'starttime' => tmp_endtime, 'endtime' => tmp_starttime }
      end
      index +=1
    end
    return day_stops,travelSegments
  end

  def gpsTotalTime(starttime, endtime, max_gap)

    time_diff = endtime.to_i - starttime.to_i
    if ( time_diff < max_gap)
      return time_diff
    end

    gps_data = self.gps_datas.where("timestamp > ? and timestamp < ?", starttime , endtime).order(:timestamp).select(:timestamp)
    total_time = 0
    last_reading = nil
    gps_data.each do |reading|
      if not last_reading.nil?
        time_diff = reading.timestamp - last_reading.timestamp
        if  time_diff < max_gap
          total_time += time_diff
        end
      end
      last_reading = reading
    end
    return total_time
  end

  def getMyDataGapsWithDayBreak(starttime=nil, endtime=nil, max_gap=5.minute)
    dataGaps = []
    binary, fullListDays = self.validatedDays(:listBinaryDays)
    fullListDays.each do |day|
      day_to_evaluate = DateTime.parse(day).at_midnight
      gps_data = self.gps_datas.where("timestamp > ? and timestamp < ?", day_to_evaluate , day_to_evaluate + 1.day).order(:timestamp).select("timestamp, lat, lon")
      last_reading = nil
      gps_data.each do |reading|
        if !last_reading.nil?
          time_diff = reading.timestamp - last_reading.timestamp
          if time_diff > max_gap
            dataGaps << [last_reading.timestamp, reading.timestamp, time_diff, last_reading.lat, last_reading.lon, reading.lat, reading.lon]
          end
        else
          time_diff = reading.timestamp - day_to_evaluate
          if time_diff > max_gap
            dataGaps << [day_to_evaluate, reading.timestamp, time_diff, nil, nil, reading.lat, reading.lon]
          end
        end
        last_reading = reading
      end
      if !last_reading.nil?
        time_diff = Time.parse( (day_to_evaluate+1.day).to_s ) - last_reading.timestamp
        if time_diff > max_gap
          dataGaps << [last_reading.timestamp, day_to_evaluate, time_diff, last_reading.lat, last_reading.lon, nil, nil]
        end
      end
    end
    return dataGaps
  end


  def getMyDataGaps(starttime=nil, endtime=nil, max_gap=5.minute)
    dataGaps = []
    if starttime.nil? or endtime.nil?
      return dataGaps
    end

    gps_data = self.gps_datas.where("timestamp > ? and timestamp < ? and accuracy <= ? and lat!=0.0 and altitude <= ?", starttime , endtime, MAX_GPS_ACCURACY, MAX_ALT).order(:timestamp).select("timestamp, lat, lon")
    last_reading = nil
    gps_data.each do |reading|
      if !last_reading.nil?
        time_diff = reading.timestamp - last_reading.timestamp
        if time_diff > max_gap
          dataGaps << [last_reading.timestamp, reading.timestamp, time_diff, last_reading.lat, last_reading.lon, reading.lat, reading.lon]
        end
      else
        time_diff = reading.timestamp - starttime
        if time_diff > max_gap
          dataGaps << [starttime, reading.timestamp, time_diff, nil, nil, reading.lat, reading.lon]
        end
      end
      last_reading = reading
    end
    if !last_reading.nil?
      time_diff = endtime - last_reading.timestamp
      if time_diff > max_gap
        dataGaps << [last_reading.timestamp, endtime, time_diff, last_reading.lat, last_reading.lon, nil, nil]
      end
    end
    return dataGaps
  end



  def getTravelActivityInfo(g_starttime, g_endtime)
    stop_list = self.stop.where("endtime >= ? and starttime <= ?", g_starttime, g_endtime).select("starttime, endtime, id")
    no_of_stops = stop_list.length
    if no_of_stops == 0
      return "Travel", "N/A"
    elsif no_of_stops == 1
      #check situation
      if stop_list[0].starttime < g_starttime and stop_list[0].endtime > g_endtime
        return "Activity", stop_list[0].id
      elsif stop_list[0].starttime > g_starttime and stop_list[0].endtime < g_endtime
        return "Activity all inside datagap => user generated activity (?)", stop_list[0].id
      elsif stop_list[0].starttime > g_starttime
        return "Travel -> Activity", stop_list[0].id
      else
        return "Activity -> Travel", stop_list[0].id
      end
    elsif no_of_stops > 1
      source_list = ""
      stop_list.each do |s|
        source_list = source_list + "|" + s.id.to_s
      end
      return "Travel and Activities", source_list
    else
      return "N/A", "N/A"
    end
  end

  def getBatteryInfo(starttime, endtime)
    initial = ActiveRecord::Base.connection.select_one("select timestamp, level from batt_datas where user_id = #{self.id} order by ABS(TIMESTAMPDIFF(SECOND,timestamp, '#{starttime}'))")
    ending = ActiveRecord::Base.connection.select_one("select timestamp, level from batt_datas where user_id = #{self.id} order by ABS(TIMESTAMPDIFF(SECOND,timestamp, '#{endtime}'))")
    return initial, ending
  end

  def gpsGap?(starttime, endtime, max_gap, time_window = nil)
    if not time_window.nil?
      if (starttime < time_window[:starttime] and endtime < time_window[:starttime]) or (starttime > time_window[:endtime] and endtime > time_window[:endtime])
        return false
      else
        if starttime < time_window[:starttime]
          starttime = time_window[:starttime]
        end
        if endtime > time_window[:endtime]
          endtime = time_window[:endtime]
        end
      end
    end

    gps_data = self.gps_datas.where("timestamp > ? and timestamp < ?", starttime , endtime).order(:timestamp).select(:timestamp)
    if (gps_data.nil? or gps_data.empty?) and (endtime.to_i - starttime.to_i) > max_gap
      return true
    end
    if !gps_data.nil? and !gps_data.empty? and (gps_data[0].timestamp.to_i - starttime.to_i > max_gap || endtime.to_i - gps_data[gps_data.length - 1].timestamp.to_i > max_gap)
      return true
    end
    last_reading = nil
    gps_data.each do |reading|
      if !last_reading.nil?
        if reading.timestamp - last_reading.timestamp > max_gap
          return true
        end
      end
      last_reading = reading
    end
    return false
  end

  def acceptDay(date_str, max_gap, hour_window=nil)
  	if (self.stop.first.starttime.strftime("%-d-%-m-%Y") == date_str)
  		return true
  	end

    day_stops, travelSegments = getTravelSegments(date_str)
    if day_stops.nil? or day_stops.empty?
      return false
    end

    if not hour_window.nil?
      time_window = {}
      time_window[:starttime] = DateTime.parse("#{date_str} #{hour_window[:starttime]}").in_time_zone(self.time_zone)
      time_window[:endtime] = DateTime.parse("#{date_str} #{hour_window[:endtime]}").in_time_zone(self.time_zone)
    end

    travelSegments.each do |ts|
      if gpsGap?(ts["starttime"], ts["endtime"], max_gap, time_window)
        return false
      end
    end
    return true
  end

  def getLocationTimeCounter(date_str, hour_window=nil, location_continuous_thr=30.minute)
    total_time_secs = 0
    day_stops, travelSegments = getTravelSegments(date_str)

    if day_stops.nil? or day_stops.empty?
      return total_time_secs
    end

    if not hour_window.nil?
      time_window = {}
      time_window[:starttime] = DateTime.parse(date_str+"  "+ hour_window[:starttime]).in_time_zone(self.time_zone)
      time_window[:endtime] = DateTime.parse(date_str+"  "+ hour_window[:endtime]).in_time_zone(self.time_zone)
    end

    day_stops.each do |stop|
      #st_time = stop.replaced_starttime.nil? ? stop.starttime : stop.replaced_starttime
      #en_time = stop.replaced_endtime.nil? ? stop.endtime : stop.replaced_endtime
      st_time = Time.parse(stop.starttime1)
      en_time = Time.parse(stop.endtime1)
      curr_time =  Time.parse(date_str).at_midnight

      if (st_time < curr_time)
        st_time = curr_time
      end

      if (en_time > curr_time+1.day-1.second)
        en_time = curr_time+1.day-1.second
      end

      time_diff = en_time - st_time
      total_time_secs += time_diff unless time_diff < 0
    end

    travelSegments.each do |ts|
      travelSegmentTime =  gpsTotalTime(ts["starttime"], ts["endtime"], location_continuous_thr)
      total_time_secs += travelSegmentTime
    end

    return total_time_secs

  end

  def getTodaysProfile
    starttime = DateTime.now
    endtime = DateTime.new(starttime.year, starttime.month, starttime.day, 23,59,59)

    profile = {
      :period0 => {
        :starttime => starttime.to_time.to_i,
        :endtime => endtime.to_time.to_i,
        :sleeping => 120,
        :collecting => 180
      }
    }
    return profile
  end

  def surveyID
    household_lta = self.household.household_lta_id
    person_lta = self.user_lta_person
    surveyid = "--"
    if ( (not household_lta.nil?) && (not person_lta.nil?))
      surveyid = "#{household_lta.postal_code}#{household_lta.lta_household}#{person_lta.lta_person}"
    end
    return surveyid
  end

  def level(points=self.points)
    if points > 500
      return 5
    elsif points >= 320
      return 4
    elsif points >= 180
      return 3
    elsif points >= 50
      return 2
    else
      return 1
    end
  end

  def points_from_data(days_collected, days_validated)
    return 10 * days_collected + 10 * days_validated
  end

  def show_timezone_warnings?
  	property = self.user_property.where(name: "dismiss_timezone_warnings").first
  	if !property.nil? and property.value == "1"
  		return false
  	end
  	return true
  end

	def time_zone_check
	  if time_zone.nil? || time_zone.empty? || ActiveSupport::TimeZone.new(time_zone).nil?
	    errors.add(:time_zone, "is not valid")
	  end
	end

  def email_or_username_must_exist
    return if email.present? || username.present?
    errors.add(:email, "Please fill in a valid Email or Username for login purposes.")
  end

  ### MaaS UPDATES ###

  # Returns validated, non-deleted stops for n distinct days.
  # NOTE: For a given day, all stops have to be validated
  def stops_for_n_days(number_of_days)
    # Get all stops
    all_stops = stop.where(deleted: false).reorder(:endtime)
    return [] if all_stops.blank? or all_stops.count('DISTINCT date(endtime)') < number_of_days

    # Set date range
    date_range = all_stops.pluck('DISTINCT date(endtime)')

    # Iterate through date_range to eliminate non-validated days
    final_date_range = []
    date_range.each do |date|
      final_date_range << date unless all_stops.exists?(
        ["date(endtime) = ? and validated != ? and mode_id != ?", date, 3, Mode::DEFAULT_MODE]) # Overnight
    end

    # Return if not enough days
    return [] if final_date_range.length < number_of_days

    # Get stops for date range
    all_stops.where(["date(endtime) in (?)", final_date_range.first(number_of_days)])
  end

  def number_of_properly_validated_days
    # Get all stops
    all_stops = stop.where(deleted: false).reorder(:endtime)
    return 0 if all_stops.blank?

    # Set date range
    date_range = all_stops.pluck('DISTINCT date(endtime)')

    # Iterate through date_range to eliminate non-validated days
    final_date_range = []
    date_range.each do |date|
      final_date_range << date unless all_stops.exists?(
        ["date(endtime) = ? and validated != ? and mode_id != ?", date, 3, Mode::DEFAULT_MODE]) # Overnight
    end

    final_date_range.length
  end

  def has_properly_validated_fully?
    number_of_properly_validated_days >= MobilityRecord::Generator::NUMBER_OF_DAYS_FOR_ANALYSIS
  end

  # Returns mobility-record trips for n distinct days
  def mobility_record_trips_for_n_days(number_of_days)
    # Get all trips
    all_trips = mobility_record_trips.order(:date_of_trip)
    return nil if all_trips.blank? or all_trips.count('DISTINCT date_of_trip') < number_of_days

    # Set date range
    # NOTE: This is being commented as we are looking for first 7 days [could be non-contiguous]
    # date_range = all_trips.first.date_of_trip .. all_trips.first.date_of_trip + (number_of_days - 1).days
    date_range = all_trips.pluck(:date_of_trip).uniq.first(number_of_days)

    # Get trips for date range
    trips = all_trips.where(date_of_trip: date_range)
  end

  # def has_any_pass?
  #   pq_response_set and
  #     pq_response_set.pq_public_transport_mobility and
  #     pq_response_set.pq_public_transport_mobility.has_any_pass?
  # end

  # def has_bus_pass?
  #   pq_response_set and
  #     pq_response_set.pq_public_transport_mobility and
  #     pq_response_set.pq_public_transport_mobility.has_free_bus?
  # end

  # def has_travel_card?
  #   pq_response_set and
  #     pq_response_set.pq_public_transport_mobility and
  #     pq_response_set.pq_public_transport_mobility.has_travel_card?
  # end

  # def travel_zones
  #   # If someone has a travelcard: take the same zones for the SP. This can feed directly from the pre-q
  #   if has_travel_card?
  #     if pq_response_set and
  #       pq_response_set.pq_public_transport_mobility and
  #       pq_response_set.pq_public_transport_mobility.travel_card_zones

  #       zones = pq_response_set.pq_public_transport_mobility.travel_card_zones_array
  #       return zones
  #     else
  #       return [1]
  #     end
  #   else
  #     # If someone doesn’t have a travelcard we should only include zones where they travel at least
  #     # 3 days a week.
  #     frequently_visited_zones
  #   end
  # end

  # def frequently_visited_zones
  #   # Get list of oyster-zones visited by user
  #   zones_list = mobility_record_trips.having('count(*) >= 3').group(:date_of_trip).map(&:oyster_zones).compact.uniq

  #   oyster_zones = []
  #   zones_list.each do |zones|
  #     oyster_zones << eval(zones)
  #   end

  #   # Flatten and convert to integer
  #   oyster_zones = oyster_zones.flatten.map(&:to_i).uniq

  #   # Remove zero/nil values
  #   oyster_zones.delete_if {|zone| zone == 0 }

  #   unless oyster_zones.blank?
  #     oyster_zones
  #   else
  #     [1]
  #   end
  # end

  def has_access_to_vehicle?
    if pre_survey_private_mobility_characteristic
      pre_survey_private_mobility_characteristic.has_access_to_vehicle?
    else
      false
    end
  end

  # def has_car?
  #   if pq_response_set and pq_response_set.pq_mobility_private
  #     (pq_response_set.pq_mobility_private.number_of_cars > 0)
  #   else
  #     false
  #   end
  # end

  # def has_bike?
  #   if pq_response_set and pq_response_set.pq_mobility_private
  #     (pq_response_set.pq_mobility_private.number_of_bicycles > 0)
  #   else
  #     false
  #   end
  # end

  # def has_used_bike_share?
  #   if pq_response_set and pq_response_set.pq_shared_mobility
  #     pq_response_set.pq_shared_mobility.used_santander_cycles?
  #   else
  #     false
  #   end
  # end

  def self.new_guest
    random_string = Time.now.to_f.to_s << rand(1000).to_s
    random_name = random_string << "@bogus.com"
    new(email: random_name,
        username: 'Guest',
        password: random_string,
        password_confirmation: random_string,
        time_zone: 'London')
  end

  def guest?
    email.include?('bogus.com')
  end

  ## PRE-SURVEY & SP EXPORT ##
  # TOURIST #
  def tourist_time_start
    created_at.in_time_zone("London").strftime("%H:%M")
  end

  def tourist_time_end
    sp_experiment.updated_at.in_time_zone("London").strftime("%H:%M")
  end

  def tourist_time_work
    if created_at.time.in_time_zone("London").between?("08:00", "18:00")
      "1"
    else
      "2"
    end
  end

  def tourist_date_started
    created_at.in_time_zone("London").strftime("%d/%m/%Y")
  end

  def tourist_day_started
    created_at.in_time_zone("London").strftime("%A")
  end

  def tourist_date_completed
    sp_experiment.updated_at.in_time_zone("London").strftime("%d/%m/%Y")
  end

  def tourist_day_completed
    sp_experiment.updated_at.in_time_zone("London").strftime("%A")
  end

  def tourist_total_dur
    sp_experiment.updated_at - created_at
  end

  def tourist_time_p1
    sp_experiment.completion_times["page_1"]
  end

  def tourist_time_p2
    sp_experiment.completion_times["page_2"]
  end

  def tourist_time_p3
    sp_experiment.completion_times["page_3"]
  end

  def tourist_time_attitudes
    sp_experiment.completion_times["page_3"]
  end

  def tourist_time_introsp
    sp_experiment.completion_times["new"]
  end

  def tourist_time_exitsp
    "-"
  end

  def tourist_browser
    browser || "Chrome"
  end

  def tourist_device
    device || "1"
  end

  def tourist_city
    ""
  end

  def tourist_country
    ""
  end

  # RESIDENT #
  alias_attribute :resident_umove_id, :umove_id

  def resident_time_start
    created_at.in_time_zone("London").strftime("%H:%M")
  end

  def resident_time_end
    sp_experiment.updated_at.in_time_zone("London").strftime("%H:%M")
  end

  def resident_time_work
    if created_at.time.in_time_zone("London").between?("08:00", "18:00")
      "1"
    else
      "2"
    end
  end

  def resident_date_started
    created_at.in_time_zone("London").strftime("%d/%m/%Y")
  end

  def resident_day_started
    created_at.in_time_zone("London").strftime("%A")
  end

  def resident_date_completed
    sp_experiment.updated_at.in_time_zone("London").strftime("%d/%m/%Y")
  end

  def resident_day_completed
    sp_experiment.updated_at.in_time_zone("London").strftime("%A")
  end

  def resident_total_dur
    sp_experiment.updated_at - created_at
  end

  # def resident_time_p1
  #   sp_experiment.completion_times["page_1"]
  # end

  # def resident_time_p2
  #   sp_experiment.completion_times["page_2"]
  # end

  # def resident_time_p3
  #   sp_experiment.completion_times["page_3"]
  # end

  # def resident_time_p4
  #   sp_experiment.completion_times["page_4"]
  # end

  # def resident_time_p5
  #   sp_experiment.completion_times["page_5"]
  # end

  # def resident_time_p6
  #   sp_experiment.completion_times["page_6"]
  # end

  # def resident_time_p7
  #   sp_experiment.completion_times["page_7"]
  # end

  # def resident_time_p8
  #   sp_experiment.completion_times["page_8"]
  # end

  # def resident_time_p9
  #   sp_experiment.completion_times["page_9"]
  # end
  def resident_time_introsp
    sp_experiment.completion_times["new"]
  end

  def resident_time_attitudessp
    sp_experiment.completion_times["attitudes"]
  end

  def resident_time_exitsp
    "-"
  end

  def resident_time_exit
    "-"
  end

  def resident_browser
    browser || "Chrome"
  end

  def resident_device
    device || "1"
  end

  def resident_step_2
    "-"
  end

  # Date the participant started the survey. DD/MM/YY
  def date_started
    created_at.strftime("%d/%m/%Y")
  end

  # Date the participant completed the survey. DD/MM/YY
  def date_completed
    sp_experiment.updated_at.strftime("%d/%m/%Y")
  end

  # time that took the participant to finish the whole survey (both Part 1 & Part 2). In seconds
  def total_dur
    # Difference between SP-completion and User-creation
    sp_experiment.updated_at - created_at
  end

  ### + RESIDENT + ###
  def resident_pre_survey_completed?
    raise "Invalid user_type" if user_type != "resident"
    pre_survey_travel_pattern ? true : false
  end

  def resident_sp_completed?
    raise "Invalid user_type" if user_type != "resident"
    (sp_experiment and sp_experiment.attitudes) ? true : false
  end

  def resident_survey_completed?
    raise "Invalid user_type" if user_type != "resident"
    (resident_pre_survey_completed? and resident_sp_completed?)
  end

  # AGE GROUP #
  def age_group_18_30?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.age >= 18 and pre_survey_socio_economic_characteristic.age <= 30)
  end

  def age_group_31_45?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.age >= 31 and pre_survey_socio_economic_characteristic.age <= 45)
  end

  def age_group_46_60?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.age >= 46 and pre_survey_socio_economic_characteristic.age <= 60)
  end

  def age_group_61?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.age >= 61)
  end

  # GENDER #
  def gender_female?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.sex == "2")
  end

  def gender_male?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_socio_economic_characteristic.sex == "1")
  end

  # DRIVING LICENCES #
  def driving_licence_car?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_private_mobility_characteristic.pliccarfull == 1)
  end

  def driving_licence_moto?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_private_mobility_characteristic.plicmotfull == 1)
  end

  def driving_licence_none?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_private_mobility_characteristic.plicnone == 1)
  end

  # CAR SHARING #
  def car_sharing_yes?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_shared_mobility_characteristic.pccmember == 1)
  end

  def car_sharing_no?
    raise "Invalid user_type" if user_type != "resident"
    (pre_survey_shared_mobility_characteristic.pccmember != 1)
  end

  ### - RESIDENT - ###


  ### + TOURIST + ###
  def tourist_pre_survey_completed?
    raise "Invalid user_type" if user_type != "tourist"
    pre_survey ? true : false
  end

  def tourist_sp_completed?
    raise "Invalid user_type" if user_type != "tourist"
    (sp_experiment and sp_experiment.attitudes) ? true : false
  end

  def tourist_survey_completed?
    raise "Invalid user_type" if user_type != "tourist"
    (tourist_pre_survey_completed? and tourist_sp_completed?)
  end
  ### - TOURIST - ###

end
