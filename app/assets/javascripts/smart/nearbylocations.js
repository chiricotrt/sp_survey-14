var smart;
if (smart === undefined) {
	smart = {};
}

smart.NearbyLocations = {};
smart.NearbyLocations._loadedMarkers = [];
smart.NearbyLocations._placeholder = null;
smart.NearbyLocations.createMarker = function(map, lat, lng){
	var marker_pos = new google.maps.LatLng(lat,lng);
	var marker = new google.maps.Marker({
						position: marker_pos,
						map: map,
						draggable: false,
						icon: '/assets/report/blue_dot.png'
						//shadow: '/images/shadow.png',
    });
    return marker;
};

smart.NearbyLocations.createInfoWindow = function(marker, venue){
	var contentString = '<div>';
	if (venue.categories.length > 0 && "icon" in venue.categories[0]){
		contentString += '<img src="'+venue.categories[0].icon.prefix+'bg_32'+venue.categories[0].icon.suffix+'"/>';
	}
	contentString += '&nbsp; '+venue.name+'</div>';

	marker.infowindow = new google.maps.InfoWindow({ content: contentString });

	google.maps.event.addListener(marker, 'mouseover', function(event) {
		marker.infowindow.open(marker.map, marker);
	});

	google.maps.event.addListener(marker, 'mouseout', function() {
		marker.infowindow.close();
	});
};

smart.NearbyLocations.unload = function(){
	console.log("unloading nearby locations!"); //, smart.NearbyLocations._loadedMarkers);
	for(var i=0;i<smart.NearbyLocations._loadedMarkers.length;i++){
		var marker = smart.NearbyLocations._loadedMarkers[i];
		google.maps.event.clearInstanceListeners(marker);
		marker.setMap(null);
	}
};


smart.NearbyLocations.load = function (row_id){
	console.log("unloading nearby locations!");
	if (smart.NearbyLocations._loadedMarkers.length > 0){
		smart.NearbyLocations.unload();
	}
	if (smart.NearbyLocations._placeholder != null){
		smart.NearbyLocations._placeholder.stop(true, true).animate({opacity:0}, 300);
	}

	// console.log(row_id);
	var marker = smart.Map.getMarkerForStop(parseInt(row_id, 10));
	// console.log(marker);

	if (marker != null){

		var whatsNearbyElm = jQuery('#'+row_id+" .nearby_marker");

		setTimeout(function(){
			// get rid of the waiting sign after 5 seconds
			if (whatsNearbyElm.text() === "Searching nearby...") {
				whatsNearbyElm.text("Server is unavailable");
				whatsNearbyElm.stop(true, true).animate({opacity:0.75}, 300);
			}

		}, 5000);
		smart.externalServices.runService('foursquare_venues', [marker.getPosition().lat(),marker.getPosition().lng()],
			function(data){
				if (row_id != whichAccordionIsOpen() || isRowTravel()) return;

				if (!("venues" in data)) {
					console.log("no venue found data:",data);

					whatsNearbyElm.text("No nearby places found");
					whatsNearbyElm.stop(true, true).animate({opacity:0.75}, 300);
					return;
				}

				var nameList = '<span class="nearby_title">Nearby places:</span>';

				var maxC = Math.min(data.venues.length, 5);

				places = "";
				for (var i=0;i<maxC;i++){
					var mk = smart.NearbyLocations.createMarker(smart.Map._map, data.venues[i].location.lat, data.venues[i].location.lng);
					smart.NearbyLocations.createInfoWindow(mk, data.venues[i]);
					smart.NearbyLocations._loadedMarkers.push(mk);
					nameList += '<span class="nearby_item">'+data.venues[i].name + '</span>';
					places += data.venues[i].name + ":" + data.venues[i].location.lat + ":" + data.venues[i].location.lng + "|";
				}
				$("#activity_details_nearby_places_" + row_id).val(places);

				whatsNearbyElm.attr('title', '');
				whatsNearbyElm.text("What's nearby");
				whatsNearbyElm.tooltip({});
				whatsNearbyElm.tooltip({content: nameList});
				whatsNearbyElm.tooltip("option", "position", {	my: "left+15 middle", at: "right bottom"});
				whatsNearbyElm.stop(true, true).animate({opacity:1}, 300);

				smart.NearbyLocations._placeholder = whatsNearbyElm;
			});
	}
};
