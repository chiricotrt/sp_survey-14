## MAIN CALL ##
jQuery ->
  setAnswers()
  setDependencies()

## SETTING ANSWERS ##
setAnswers = ->
  # reasons_for_not_using
  # if $("#pre_survey_public_transport_characteristic_frequency_of_usage").val() == "1" or $("#pre_survey_public_transport_characteristic_frequency_of_usage").val() == "2"
  if $("#pre_survey_public_transport_characteristic_frequency_of_usage").val() in ["1", "2", "3", "4"]
    $(".pre_survey_public_transport_characteristic_reasons_for_not_using").show()

  # public_transport_passes
  if $("#pre_survey_public_transport_characteristic_frequency_of_usage").val() and $("#pre_survey_public_transport_characteristic_frequency_of_usage").val() != "1"
    $(".pre_survey_public_transport_characteristic_pass_holder").show()

  # pass-related questions
  if $("#pre_survey_public_transport_characteristic_pass_holder_1").prop("checked")
    $(".pass_questions").show()
  else
    $(".pass_questions").hide()

## SETTING DEPENDENCIES ##
setDependencies = ->
  $("#pre_survey_public_transport_characteristic_frequency_of_usage").change ->
    if !$(this).val() # frequency_of_usage = <blank>
      $(".pre_survey_public_transport_characteristic_reasons_for_not_using").hide()
      $(".pre_survey_public_transport_characteristic_pass_holder").hide()
    else if $(this).val() == "1"
      $(".pre_survey_public_transport_characteristic_reasons_for_not_using").show()
      $(".pre_survey_public_transport_characteristic_pass_holder").hide()
    else if $(this).val() == "2" or $(this).val() == "3" or $(this).val() == "4"
      $(".pre_survey_public_transport_characteristic_reasons_for_not_using").show()
      $(".pre_survey_public_transport_characteristic_pass_holder").show()
    else # frequency_of_usage = others
      $(".pre_survey_public_transport_characteristic_reasons_for_not_using").hide()
      $(".pre_survey_public_transport_characteristic_pass_holder").show()

  # if pass_holder yes/no is clicked, show/hide
  $("#pre_survey_public_transport_characteristic_pass_holder_1").click ->
    $(".pass_questions").show()

  $("#pre_survey_public_transport_characteristic_pass_holder_0").click ->
    $(".pass_questions").hide()